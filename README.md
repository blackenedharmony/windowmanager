# Solaire's Dotfiles

[![Based License](https://custom-icon-badges.herokuapp.com/badge/BASED_LICENSE-696969?logo=gigachad&style=for-the-badge)](https://github.com/thatonecalculator/BASED-LICENSE)

basic ass dotfiles for i3, picom, polybar, etc.

you can figure out the dependencies by just checking the configs

if you saw buckle in them and were confused about it:
buckle is literally just a program that makes buckling-spring noise when you type. that's it

don't forget to edit your keymap, refresh rate etc. according to your setup
it's pretty simple to figure out, defaults are for my T440p (60 Hz display)
